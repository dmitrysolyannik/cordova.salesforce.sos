/* global cordova:false */
/* globals window */

/*!
 * Module dependencies.
 */

window.SalesforceSOS = function(email) {
	var exec = cordova.require('cordova/exec');
	exec(function() {}, function(err) {alert(err);}, "SOS", "startSOS", [email]);
};